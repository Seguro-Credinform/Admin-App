import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';
import { MaterialModule } from '@angular/material';

import { Pages } from './pages.component';

@NgModule({
  imports: [CommonModule, NgaModule, MaterialModule, routing],
  declarations: [Pages]
})
export class PagesModule {
}
