/**
 * Created by Desarrollo KJ on 28/4/2017.
 */
import {Component, NgZone, Inject, EventEmitter} from "@angular/core";
import {NgUploaderOptions} from "ngx-uploader";
import {EmergenciaService} from "./../../../../services/emergencias.services";
import {Configuration} from "./../../../../services/config.service";
import {Emergencia, Error} from "./../../../../models";
import {ToastyService} from 'ng2-toasty';
import * as _ from "lodash";
declare var $: any;
declare var _toastyService: any;

@Component({
  selector: 'emergencias',
  templateUrl: 'emergencias.html'
})

export class EmergenciasComponent {

  public control:boolean = true;

  public emergencia:Emergencia = {
    idEmergencia: 0,
    telefono: "",
    correo: "",
    nombre: "",
    imagen: [{
      grande:'',
      mediana:'',
      pequena:''
    }],
    status: 1
  };

  public listEmergencias:Emergencia[] = [];

  //Manejo de imagenes
  public options: NgUploaderOptions;
  public response: any;
  public hasBaseDropZoneOver: boolean;
  public imagen: string = '';
  public startUploadEvent: EventEmitter<string>;

  public _configuration = new Configuration();
  public _actionUrl = this._configuration.ServerWithApiUrl + 'emergencias/';
  public _header = this._configuration.customHeader;


  constructor(private _toastyService: ToastyService, @Inject(NgZone) private zone: NgZone, private _emergenciasService:EmergenciaService) {
    this.getAllEmergencias();
    this.startUploadEvent = new EventEmitter<string>();
  }

  public nuevo(): void {
    this.control = false;
    this.emergencia = {
      idEmergencia: 0,
      telefono: "",
      correo: "",
      nombre: "",
      imagen: [{
        grande: '',
        mediana: '',
        pequena: ''
      }],
      status: 1
    }
  }

  public cancelar(): void {
    this.control = true;
    this.emergencia = {
      idEmergencia: 0,
      telefono: "",
      correo: "",
      nombre: "",
      imagen: [{
        grande: '',
        mediana: '',
        pequena: ''
      }],
      status: 1
    };
  }

  public editar(emergencia:Emergencia):void {
    this.control = false;
    this.emergencia = emergencia;
    this.imagen = this.emergencia.imagen[0].pequena;
    this.options = new NgUploaderOptions({
      url: this._actionUrl+this.emergencia.idEmergencia+'/imagenes',
      customHeaders:this._header,
      fieldName : 'media',
      method: 'POST',
      autoUpload: false,
      calculateSpeed: true
    });
  }

  public guardar(): void {
    delete this.emergencia.imagen;
    if (this.emergencia.idEmergencia > 0) {
      this._emergenciasService.edit(this.emergencia).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          this.getAllEmergencias();
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
    else {
      this._emergenciasService.save(this.emergencia).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          let emergencia:Emergencia = _.cloneDeep(this.emergencia);
          this.emergencia = result;
          this.getAllEmergencias();
          this.options = new NgUploaderOptions({
            url: this._actionUrl+this.emergencia.idEmergencia+'/imagenes',
            customHeaders:this._header,
            fieldName : 'media',
            method: 'POST',
            autoUpload: false,
            calculateSpeed: true
          });
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
  }

  public eliminar(emergencia: Emergencia): void {
    this._emergenciasService.delete(emergencia).subscribe(
      (result) => {
        this._toastyService.success('Eliminado con exito');
        this.getAllEmergencias();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 400) {
          this._toastyService.error('No se puede borrar emergencia en uso, se recomienda desactivarlo.');
        }
        if (errorAux.status == 404) {
          this.listEmergencias.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    );
  }

  public getAllEmergencias(): void {
    this._emergenciasService.getAll().subscribe(
      (result) => {
        this.listEmergencias = result.json();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 404) {
          this.listEmergencias.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    )
  }

  public startUpload() {
    this.options.url = this._actionUrl+this.emergencia.idEmergencia+'/imagenes';
    this.startUploadEvent.emit("startUpload");
  }


  public handleUpload(data: any) {
    setTimeout(() => {
      this.zone.run(() => {
        this.response = data;

        if (data && data.response) {
          if(data.response == 'El formato que intentas subir no está permitido :'){
            this._toastyService.error(data.response);
          }
          else {
            this.emergencia.imagen[0] = JSON.parse(data.response);
            this.imagen = this.emergencia.imagen[0].pequena;
            this.getAllEmergencias();
            this._toastyService.success('Imagen subida con exito');
          }
        }
      });
    });
  }

  public handlePreviewData(data: any) {
    this.imagen = data;
  }



}
