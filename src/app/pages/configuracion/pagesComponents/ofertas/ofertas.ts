/**
 * Created by Desarrollo KJ on 28/4/2017.
 */
import {Component, NgZone, Inject, EventEmitter} from "@angular/core";
import {NgUploaderOptions} from "ngx-uploader";
import {Configuration, OfertasService} from "./../../../../services";
import {Oferta, Error} from "./../../../../models";
import * as _ from "lodash";
import {ToastyService} from 'ng2-toasty';
declare var $: any;
declare var _toastyService: any;

@Component({
  selector: 'ofertas',
  templateUrl: 'ofertas.html'
})

export class OfertasComponent {

  public control: boolean = true;

  public oferta: Oferta = {
    idOferta: 0,
    descripcion: "",
    titulo: "",
    imagen: [{
      grande: '',
      mediana: '',
      pequena: ''
    }],
    status: 1
  };

  public listOfertas: Oferta[] = [];

  //Manejo de imagenes
  public options: NgUploaderOptions;
  public response: any;
  public hasBaseDropZoneOver: boolean;
  public imagen: string = '';
  public startUploadEvent: EventEmitter<string>;

  public _configuration = new Configuration();
  public _actionUrl = this._configuration.ServerWithApiUrl + 'ofertas/';
  public _header = this._configuration.customHeader;


  constructor(private _toastyService: ToastyService, @Inject(NgZone) private zone: NgZone, private _ofertasService: OfertasService) {
    this.getAllOfertas();
    this.startUploadEvent = new EventEmitter<string>();
  }

  public nuevo(): void {
    this.control = false;
    this.oferta = {
      idOferta: 0,
      descripcion: "",
      titulo: "",
      imagen: [{
        grande: '',
        mediana: '',
        pequena: ''
      }],
      status: 1
    }
  }

  public cancelar(): void {
    this.control = true;
    this.oferta = {
      idOferta: 0,
      descripcion: "",
      titulo: "",
      imagen: [{
        grande: '',
        mediana: '',
        pequena: ''
      }],
      status: 1
    };
  }

  public editar(oferta: Oferta): void {
    this.control = false;
    this.oferta = oferta;
    this.imagen = this.oferta.imagen[0].pequena;
    this.options = new NgUploaderOptions({
      url: this._actionUrl + this.oferta.idOferta + '/imagenes',
      customHeaders: this._header,
      fieldName: 'media',
      method: 'POST',
      autoUpload: false,
      calculateSpeed: true
    });
  }

  public guardar(): void {
    delete this.oferta.imagen;
    if (this.oferta.idOferta > 0) {
      this._ofertasService.edit(this.oferta).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          this.getAllOfertas();
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
    else {
      this._ofertasService.save(this.oferta).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          let oferta: Oferta = _.cloneDeep(this.oferta);
          oferta.idOferta = result.idOferta;
          this.getAllOfertas();
          this.oferta = oferta;
          this.imagen = '';
          this.options = new NgUploaderOptions({
            url: this._actionUrl + this.oferta.idOferta + '/imagenes',
            customHeaders: this._header,
            fieldName: 'media',
            method: 'POST',
            autoUpload: false,
            calculateSpeed: true
          });
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
  }

  public eliminar(oferta: Oferta): void {
    this._ofertasService.delete(oferta).subscribe(
      (result) => {
        this._toastyService.success('Eliminado con exito');
        this.getAllOfertas();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 400) {
          this._toastyService.error('No se puede borrar oferta en uso, se recomienda desactivarlo.');
        }
        if (errorAux.status == 404) {
          this.listOfertas.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    );
  }

  public getAllOfertas(): void {
    this._ofertasService.getAll().subscribe(
      (result) => {
        this.listOfertas = result.json();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 404) {
          this.listOfertas.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    )
  }

  public startUpload() {
    this.options.url = this._actionUrl + this.oferta.idOferta + '/imagenes';
    this.startUploadEvent.emit("startUpload");
  }


  public handleUpload(data: any) {
    setTimeout(() => {
      this.zone.run(() => {
        this.response = data;

        if (data && data.response) {
          if (data.response == 'El formato que intentas subir no está permitido :') {
            this._toastyService.error(data.response);
          }
          else {
            this.oferta.imagen[0] = JSON.parse(data.response);
            this.imagen = this.oferta.imagen[0].pequena;
            this.getAllOfertas();
          }
        }
      });
    });
  }

  public handlePreviewData(data: any) {
    this.imagen = data;
  }


}
