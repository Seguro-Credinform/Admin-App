/**
 * Created by Desarrollo KJ on 28/4/2017.
 */
import {Component, NgZone, Inject, EventEmitter} from "@angular/core";
import {NgUploaderOptions} from "ngx-uploader";
import {Configuration, OficinasService} from "./../../../../services";
import {Oficina, Error} from "./../../../../models";
import * as _ from "lodash";
import {ToastyService} from 'ng2-toasty';
declare var $: any;
declare var _toastyService: any;

@Component({
  selector: 'oficinas',
  templateUrl: 'oficinas.html'
})

export class OficinasComponent {

  public control: boolean = true;

  public oficina: Oficina = {
    idOficina: 0,
    telefono: "",
    correo: "",
    nombre: "",
    direccion:"",
    status: 1
  };

  public listOficinas: Oficina[] = [];

  //Manejo de imagenes
  public options: NgUploaderOptions;
  public response: any;
  public hasBaseDropZoneOver: boolean;
  public imagen: string = '';
  public startUploadEvent: EventEmitter<string>;

  public _configuration = new Configuration();
  public _actionUrl = this._configuration.ServerWithApiUrl + 'oficinass/';
  public _header = this._configuration.customHeader;


  constructor(private _toastyService: ToastyService, @Inject(NgZone) private zone: NgZone, private _oficinasService: OficinasService) {
    this.getAllOficinas();
    this.startUploadEvent = new EventEmitter<string>();
  }

  public nuevo(): void {
    this.control = false;
    this.oficina = {
      idOficina: 0,
      telefono: "",
      correo: "",
      nombre: "",
      direccion:"",
      status: 1
    }
  }

  public cancelar(): void {
    this.control = true;
    this.oficina = {
      idOficina: 0,
      telefono: "",
      correo: "",
      nombre: "",
      direccion:"",
      status: 1
    };
  }

  public editar(oficina: Oficina): void {
    this.control = false;
    this.oficina = oficina;
  }

  public guardar(): void {
    if (this.oficina.idOficina > 0) {
      this._oficinasService.edit(this.oficina).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          this.getAllOficinas();
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
    else {
      this._oficinasService.save(this.oficina).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          let oficinas: Oficina = _.cloneDeep(this.oficina);
          oficinas.idOficina = result.idOficina;
          this.getAllOficinas();
          this.oficina = oficinas;
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
  }

  public eliminar(oficinas: Oficina): void {
    this._oficinasService.delete(oficinas).subscribe(
      (result) => {
        this._toastyService.success('Eliminado con exito');
        this.getAllOficinas();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 400) {
          this._toastyService.error('No se puede borrar oficinas en uso, se recomienda desactivarlo.');
        }
        if (errorAux.status == 404) {
          this.listOficinas.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    );
  }

  public getAllOficinas(): void {
    this._oficinasService.getAll().subscribe(
      (result) => {
        this.listOficinas = result.json();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 404) {
          this.listOficinas.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    )
  }

}
