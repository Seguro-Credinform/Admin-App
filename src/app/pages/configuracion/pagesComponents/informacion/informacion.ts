import {Component, NgZone, Inject, EventEmitter} from "@angular/core";
import {NgUploaderOptions} from "ngx-uploader";
import {Configuration, InformacionesService} from "./../../../../services";
import {Informacion, Error} from "./../../../../models";
import * as _ from "lodash";
import {ToastyService} from 'ng2-toasty';
declare var $: any;
declare var _toastyService: any;

@Component({
  selector: 'informacion',
  templateUrl: 'informacion.html'
})

export class InformacionComponent {

  public control:boolean = true;

  public informacion:Informacion = {
    idInformacion: 0,
    descripcion: "",
    titulo: "",
    imagen: [{
      grande:'',
      mediana:'',
      pequena:''
    }],
    status: 1
  };

  public listInformaciones:Informacion[] = [];

  //Manejo de imagenes
  public options: NgUploaderOptions;
  public response: any;
  public hasBaseDropZoneOver: boolean;
  public imagen: string = '';
  public startUploadEvent: EventEmitter<string>;

  public _configuration = new Configuration();
  public _actionUrl = this._configuration.ServerWithApiUrl + 'informaciones/';
  public _header = this._configuration.customHeader;


  constructor(private _toastyService: ToastyService, @Inject(NgZone) private zone: NgZone, private _informacionesService:InformacionesService) {
    this.getAllInformacions();
    this.startUploadEvent = new EventEmitter<string>();
  }

  public nuevo(): void {
    this.control = false;
    this.informacion = {
      idInformacion: 0,
      descripcion: "",
      titulo: "",
      imagen: [{
        grande: '',
        mediana: '',
        pequena: ''
      }],
      status: 1
    }
  }

  public cancelar(): void {
    this.control = true;
    this.informacion = {
      idInformacion: 0,
      descripcion: "",
      titulo: "",
      imagen: [{
        grande: '',
        mediana: '',
        pequena: ''
      }],
      status: 1
    };
  }

  public editar(informacion:Informacion):void {
    this.control = false;
    this.informacion = informacion;
    this.imagen = this.informacion.imagen[0].pequena;
    this.options = new NgUploaderOptions({
      url: this._actionUrl+this.informacion.idInformacion+'/imagenes',
      customHeaders:this._header,
      fieldName : 'media',
      method: 'POST',
      autoUpload: false,
      calculateSpeed: true
    });
  }

  public guardar(): void {
    delete this.informacion.imagen;
    if (this.informacion.idInformacion > 0) {
      this._informacionesService.edit(this.informacion).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          this.getAllInformacions();
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
    else {
      this._informacionesService.save(this.informacion).subscribe(
        (result) => {
          this._toastyService.success('Guardado con exito');
          let informacion:Informacion = _.cloneDeep(this.informacion);
          this.getAllInformacions();
          this.informacion = result;
          this.imagen = '';
          this.options = new NgUploaderOptions({
            url: this._actionUrl+this.informacion.idInformacion+'/imagenes',
            customHeaders:this._header,
            fieldName : 'media',
            method: 'POST',
            autoUpload: false,
            calculateSpeed: true
          });
        },
        (erro) => {
          this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
  }

  public eliminar(informacion: Informacion): void {
    this._informacionesService.delete(informacion).subscribe(
      (result) => {
        this._toastyService.success('Eliminado con exito');
        this.getAllInformacions();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 400) {
          this._toastyService.error('No se puede borrar informacion en uso, se recomienda desactivarlo.');
        }
        if (errorAux.status == 404) {
          this.listInformaciones.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    );
  }

  public getAllInformacions(): void {
    this._informacionesService.getAll().subscribe(
      (result) => {
        this.listInformaciones = result.json();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 404) {
          this.listInformaciones.length = 0;
        }
        else {
          this._toastyService.error('Error con el servidor o conexión');
        }
      }
    )
  }

  public startUpload() {
    this.options.url = this._actionUrl+this.informacion.idInformacion+'/imagenes';
    this.startUploadEvent.emit("startUpload");
  }


  public handleUpload(data: any) {
    setTimeout(() => {
      this.zone.run(() => {
        this.response = data;

        if (data && data.response) {
          if(data.response == 'El formato que intentas subir no está permitido :'){
            this._toastyService.error(data.response);
          }
          else {
            this.informacion.imagen[0] = JSON.parse(data.response);
            this.imagen = this.informacion.imagen[0].pequena;
            this.getAllInformacions();
          }
        }
      });
    });
  }

  public handlePreviewData(data: any) {
    this.imagen = data;
  }



}
