/**
 * Created by Desarrollo KJ on 28/4/2017.
 */
import {Component} from "@angular/core";
import {ToastyService} from "ng2-toasty";
import {ConfiguracionesService} from "./../../../../services";
import {Configuracion} from "./../../../../models";


declare var $: any;

@Component({
  selector: 'generales',
  templateUrl: 'generales.html'
})

export class GeneralesComponent {

  public configuracion: Configuracion = {
    idConfiguracion: 1,
    iva: 0,
    montoMaximoTransaccionRapida: 0,
    tiempoAtencionReferencial: 0
  };

  constructor(private _toastyService: ToastyService, private _configuracionesServices: ConfiguracionesService) {
    this._configuracionesServices.get().subscribe(
      (result)=>{
        this.configuracion = result.json()[0];
      }
    );
  }

  public guardar():void{
      this._configuracionesServices.edit(this.configuracion).subscribe(
        (result)=>{
          console.log(result);
          if(result[0]==1){
            this._toastyService.success('Se guardo con exito');
          }
          else {
            this._toastyService.info('No hubo cambios');
          }
        },
        (err)=>{
          this._toastyService.error('Error en el servicios o conexión');
        }
      );
  }

}
