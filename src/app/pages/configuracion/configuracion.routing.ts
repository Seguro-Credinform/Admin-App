import {Routes, RouterModule}  from '@angular/router';
import {ConfiguracionComponent} from './configuracion.component';

const routes: Routes = [
  {
    path: '',
    component: ConfiguracionComponent,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);
