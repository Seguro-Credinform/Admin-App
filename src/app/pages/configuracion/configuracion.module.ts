import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {NgaModule} from "../../theme/nga.module";
import {PaginationModule} from "ng2-bootstrap/pagination";
import { ToastyModule } from 'ng2-toasty';
import {TabsModule} from "ng2-bootstrap/tabs";
import {TooltipModule} from "ng2-bootstrap/tooltip";
import {NgUploaderModule} from "ngx-uploader";
import {ConfiguracionComponent} from "./configuracion.component";
import {EmergenciasComponent} from "./pagesComponents/emergencias";
import {OficinasComponent} from "./pagesComponents/oficinas";
import {InformacionComponent} from "./pagesComponents/informacion";
import {OfertasComponent} from "./pagesComponents/ofertas";
import {routing} from "./configuracion.routing";
import {
  Configuration,
  ConfiguracionesService,
  EmergenciaService,
  InformacionesService,
  OficinasService,
  OfertasService
} from "./../../services";
import {CKEditorModule} from "ng2-ckeditor";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    NgUploaderModule,
    CKEditorModule,
    ToastyModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    ConfiguracionComponent,
    EmergenciasComponent,
    InformacionComponent,
    OficinasComponent,
    OfertasComponent,
  ],
  providers: [
    ConfiguracionesService,
    EmergenciaService,
    InformacionesService,
    OficinasService,
    OfertasService,
    Configuration
  ]
})


export class ConfiguracionModule {
}
