import { Routes, RouterModule }  from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module'
  },
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule' },
      { path: 'ui', loadChildren: 'app/pages/ui/ui.module#UiModule' },
      { path: 'atencion-cliente', loadChildren: 'app/pages/atencionCliente/atencionCliente.module#AtencionClienteModule' },
      { path: 'centro-mensajes', loadChildren: 'app/pages/centroMensajes#CentroMensajesModule' },
      { path: 'configuracion', loadChildren: 'app/pages/configuracion/configuracion.module#ConfiguracionModule' },
      { path: 'siniestro', loadChildren: 'app/pages/reportes/pedidos.module' }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
