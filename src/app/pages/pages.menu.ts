export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'atencion-cliente',
        data: {
          menu: {
            title: 'Atención al Cliente',
            icon: 'fa fa-headphones',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
        children: [
          {
            path: 'chat',
            data: {
              menu: {
                title: 'Chat',
              }
            }
          }
        ]
      },
      {
        path: 'siniestro',
        data: {
          menu: {
            title: 'Reportes de Siniestro',
            icon: 'fa fa-car',
            selected: false,
            expanded: false,
            order: 300,
          }
        }
      },
      {
        path: 'centro-mensajes',
        data: {
          menu: {
            title: 'Centro de Mensajes',
            icon: 'fa fa-envelope',
            selected: false,
            expanded: false,
            order: 350
          }
        }
      },
      {
        path: 'configuracion',
        data: {
          menu: {
            title: 'Configuracion',
            icon: 'fa fa-cog',
            selected: false,
            expanded: false,
            order: 300,
          }
        }
      }
    ]
  }
];
