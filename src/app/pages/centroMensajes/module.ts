import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {NgaModule} from "../../theme/nga.module";
import {PaginationModule} from "ng2-bootstrap/pagination";
import {TabsModule} from "ng2-bootstrap/tabs";
import {TooltipModule} from "ng2-bootstrap/tooltip";
import {CentroMensajeComponent} from "./component";
import {MensajesComponent} from "./pagesComponents/mensajes";
import {routing} from "./routing";
import {Configuration, MensajesService} from "./../../services";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    CentroMensajeComponent,
    MensajesComponent
  ],
  providers: [
    Configuration,
    MensajesService
  ]
})


export class CentroMensajesModule {
}
