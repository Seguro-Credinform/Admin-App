import {Routes, RouterModule}  from '@angular/router';
import {CentroMensajeComponent} from './component';

const routes: Routes = [
  {
    path: '',
    component: CentroMensajeComponent,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);
