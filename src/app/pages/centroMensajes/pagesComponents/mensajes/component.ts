/**
 * Created by Desarrollo KJ on 28/4/2017.
 */
import {Component} from "@angular/core";
import {Configuration, MensajesService} from "./../../../../services";
import {Mensaje, Error} from "./../../../../models";
import * as _ from "lodash";
declare var $: any;
declare var _toastyService: any;

@Component({
  selector: 'mensajes',
  templateUrl: 'index.html'
})

export class MensajesComponent {

  public control: boolean = true;
  public item: Mensaje = {
    idMensaje: 0,
    asunto: "",
    mensaje: "",
    respuesta: "",
    username: "",
    idTipoMensaje: 0
  };
  public list: Mensaje[] = [];

  /**
   * Filtros
   * @param itemsService
   */
  public tipo:number = 0;
  public sinResponder:number = 0;


  constructor(private itemsService: MensajesService) {
    this.getAll();
  }

  public cancelar(): void {
    this.control = true;
    this.item = {
      idMensaje: 0,
      asunto: "",
      mensaje: "",
      respuesta: "",
      username: "",
      idTipoMensaje: 0
    };
  }

  public editar(item: Mensaje): void {
    this.control = false;
    this.item = item;
  }

  public guardar(): void {
    if (this.item.idMensaje > 0) {
      this.itemsService.edit(this.item).subscribe(
        (result) => {
          ////this._toastyService.success('Guardado con exito');
          this.getAll();
        },
        (erro) => {
          //this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
    else {
      this.itemsService.save(this.item).subscribe(
        (result) => {
          //this._toastyService.success('Guardado con exito');
          let items: Mensaje = _.cloneDeep(this.item);
          items.idMensaje = result.idMensaje;
          this.getAll();
          this.item = items;
        },
        (erro) => {
          //this._toastyService.error('Error de servicio o conexion');
        }
      );
    }
  }

  public eliminar(items: Mensaje): void {
    this.itemsService.delete(items).subscribe(
      (result) => {
        //this._toastyService.success('Eliminado con exito');
        this.getAll();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 400) {
          //this._toastyService.error('No se puede borrar items en uso, se recomienda desactivarlo.');
        }
        if (errorAux.status == 404) {
          this.list.length = 0;
        }
        else {
          //this._toastyService.error('Error con el servidor o conexión');
        }
      }
    );
  }

  public getAll(): void {
    this.itemsService.getAll().subscribe(
      (result) => {
        this.list = result.json();
      },
      (error) => {
        let errorAux: Error = error.json();
        if (errorAux.status == 404) {
          this.list.length = 0;
        }
        else {
          //this._toastyService.error('Error con el servidor o conexión');
        }
      }
    )
  }

}
