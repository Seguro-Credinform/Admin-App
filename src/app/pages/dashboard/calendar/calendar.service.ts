import {Injectable} from '@angular/core';
import {BaThemeConfigProvider} from '../../../theme';

@Injectable()
export class CalendarService {

  constructor(private _baConfig:BaThemeConfigProvider) {
  }

  getData() {

    let dashboardColors = this._baConfig.get().colors.dashboard;
    return {
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      lang: 'es',
      defaultDate: '2017-04-23',
      selectable: true,
      selectHelper: true,
      editable: true,
      eventLimit: true,
      events: [
        {
          title: 'Inmobiliaria',
          start: '2017-04-15',
          color: dashboardColors.silverTree,
          textColor : '#ffffff'
        },
        {
          title: 'Seguro Médico',
          start: '2017-04-07',
          color: dashboardColors.danger,
          textColor : '#ffffff'
        },
        {
          title: 'Vehicular',
          start: '2017-04-01',
          color: dashboardColors.gossip,
          textColor : '#ffffff'
        }
      ]
    };
  }
}
