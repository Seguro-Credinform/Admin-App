import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

@Injectable()
export class PieChartService {

  constructor(private _baConfig:BaThemeConfigProvider) {
  }

  getData() {
    let pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
    return [
      {
        color: pieColor,
        description: 'Clientes Activos',
        stats: '57820',
        icon: 'fa fa-4x fa-user',
      }, {
        color: pieColor,
        description: 'Vehiculos',
        stats: '2741',
        icon: 'fa fa-4x fa-car',
      }, {
        color: pieColor,
        description: 'Inmuebles',
        stats: '1623',
        icon: 'fa fa-4x fa-home',
      }
    ];
  }
}
