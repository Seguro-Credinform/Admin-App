import {Injectable} from '@angular/core';

@Injectable()
export class TodoService {

  private _todoList = [
    { text: 'Verificar poliza Carlos Mendoza.' },
    { text: 'Llamadas Pendietes.' },
    { text: 'Solicitud Ana Hernandez.' },
    { text: 'Actualizar numero de Policia.' }
  ];

  getTodoList() {
    return this._todoList;
  }
}
