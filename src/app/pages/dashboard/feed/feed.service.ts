import {Injectable} from '@angular/core';

@Injectable()
export class FeedService {

  private _data = [
    {
      type: 'text-message',
      author: 'Kostya',
      surname: 'Danovsky',
      header: 'Nuevo Mensaje',
      text: 'Buenas tardes, quede accidentado por la circuvalación y la grua nada que llega',
      time: 'Hoy 11:55 pm',
      ago: 'hace 25 min',
      expanded: true,
    }, {
      type: 'geo-message',
      author: 'Nick',
      surname: 'Cat',
      header: 'Señal de emergencia',
      text: '"Plaza Colón, Cochabamba"',
      preview: 'app/feed/new-york-location.png',
      link: 'https://www.google.by/maps/place/New+York,+NY,+USA/@40.7201111,-73.9893872,14z',
      time: 'Hoy 11:55 pm',
      ago: 'hace 30 min',
      expanded: true,
    }, {
      type: 'video-message',
      author: 'Andrey',
      surname: 'Hrabouski',
      header: 'Actualización',
      text: '"Renovación de Poliza"',
      link: 'https://www.youtube.com/watch?v=IfcpzBbbamk',
      time: 'Hoy 9:30 pm',
      ago: 'hace 3 horas',
      expanded: true,
    }, {
      type: 'text-message',
      author: 'Nasta',
      surname: 'Linnie',
      header: 'Nuevo Mensaje',
      text: 'Me gustaria información para aumentar la cobertura de mi poliza',
      time: 'Hoy 8:30 pm',
      ago: 'hace 4 horas',
      expanded: true,
    }
  ];

  getData() {
    return this._data;
  }
}
