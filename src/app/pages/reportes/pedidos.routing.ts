import {Routes, RouterModule}  from '@angular/router';
import {PedidosComponent} from './pedidos.component';

const routes: Routes = [
  {
    path: '',
    component: PedidosComponent,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);
