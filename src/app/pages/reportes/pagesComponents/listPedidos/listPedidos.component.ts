/**
 * Created by Desarrollo KJ on 24/2/2017.
 */
import {Component} from '@angular/core';
import {Categoria} from '../../models/categoria';
import {CategoriasService} from '../../services/categorias.services';

import {ToastyService} from 'ng2-toasty';

import * as _ from "lodash";


@Component({
  selector: 'list-pedidos',
  templateUrl: 'listPedidos.html',
  styleUrls:['./style']
})
export class ListPedidos {

  public control: boolean = true;
  public status: boolean = true;
  public totalItems:number = 0;
  public itemsPerPage:number = 10;
  public currentPage:number = 1;

  arr = Array; //Array type captured in a variable
  num:number = 20;
  panelOperation: number = 1;
  action:boolean = true;

  public ventaDefault:any = [];

  constructor(private _toastyService: ToastyService) {

  }

  public openDetails(): void {
    this.control = false;
  }

  public cancelar(): void {
    this.control = true;
  }


}


