/**
 * Created by Desarrollo KJ on 22/2/2017.
 */
import {NgModule}      from '@angular/core';
import {CommonModule}  from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgaModule} from '../../theme/nga.module';
import {ToastyModule} from 'ng2-toasty';
import {PaginationModule, TooltipModule} from 'ng2-bootstrap';
import {TabsModule} from 'ng2-bootstrap/tabs';
import {routing} from './pedidos.routing';

import {PedidosComponent} from './pedidos.component';
import {ListPedidos} from './pagesComponents/listPedidos';


@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule,
    FormsModule,
    ToastyModule,
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    PaginationModule.forRoot()
  ],
  providers: [
  ],
  declarations: [
    PedidosComponent,
    ListPedidos
  ]
})
export default class PedidosModule {
}
