import { Routes, RouterModule }  from '@angular/router';

import { AtencionCliente } from './atencionCliente.component';
import { AtencionClienteChat } from './atencionClienteChat/atencionClienteChat.component';
import { Grid } from './components/grid/grid.component';
import { Icons } from './components/icons/icons.component';
import { Modals } from './components/modals/modals.component';
import { Typography } from './components/typography/typography.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: AtencionCliente,
    children: [
      { path: 'chat', component: AtencionClienteChat }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
