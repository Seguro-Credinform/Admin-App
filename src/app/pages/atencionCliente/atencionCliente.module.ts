import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { MaterialModule } from '@angular/material';

import { routing }       from './atencionCliente.routing';
import { AtencionCliente } from './atencionCliente.component';
import { AtencionClienteChat } from './atencionClienteChat/atencionClienteChat.component';

import { SocketIoModule, SocketIoConfig } from 'ng2-socket-io';
const config: SocketIoConfig = { url: 'http://192.34.63.79:8000', options: {} };


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    MaterialModule,
    SocketIoModule.forRoot(config),
    routing
  ],
  declarations: [
    AtencionCliente,
    AtencionClienteChat
  ],
  providers: [

  ]
})
export class AtencionClienteModule {
}
