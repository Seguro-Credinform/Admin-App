/**
 * Created by Desarrollo KJ on 5/4/2017.
 */
import {Component} from '@angular/core';
import { Socket } from 'ng2-socket-io';


@Component({
  templateUrl: './atencionClienteChat.html',
  selector: 'atencion-cliente-chat',
  styleUrls:['./atencionClienteChat']
})
export class AtencionClienteChat {
  arr = Array; //Array type captured in a variable
  num:number = 20;
  panelOperation: number = 1;
  action:boolean = false;
  mensaje:string= '';

  constructor(private socket: Socket) {
  }

  sendMessage(){
    this.socket.emit("mensaje", {usuario:1, mensaje:this.mensaje})
    this.mensaje = '';

    this.getMessage();
  }

  getMessage() {
    return this.socket
      .fromEvent<any>("respuesta")
      .map(data => {
        console.log(data);
      } );
  }

}
