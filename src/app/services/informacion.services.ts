import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Informacion} from '../models';
import * as _ from "lodash";

@Injectable()
export class InformacionesService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'informaciones/';
  }

  getAll(): any {
    return this._http.get(this._actionUrl+'?perPage=99', this._configuration.resquestOption)
      .map(res => res);
  }

  save(informacion:Informacion):any {
    let informacionCopy = _.cloneDeep(informacion);
    delete informacionCopy.idInformacion;
    return this._http.post(this._actionUrl, informacionCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(informacion:Informacion):any {
    let informacionCopy = _.cloneDeep(informacion);
    let id = informacion.idInformacion;
    delete informacionCopy.idInformacion;
    return this._http.put(this._actionUrl + id, informacionCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(informacion:Informacion):any {
    let id = informacion.idInformacion;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

}
