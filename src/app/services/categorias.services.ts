import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Categoria} from '../models';
import * as _ from "lodash";

@Injectable()
export class CategoriasService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'categorias/';
  }

  getAll(padre: number = null, page:number = 1, perPage:number = 10): any {
    let actionUrl = this._actionUrl+'?page='+page+'&perPage='+perPage;
    if (padre != null) {
      actionUrl += '&idCategoriaPadre=' + padre;
    }
    return this._http.get(actionUrl, this._configuration.resquestOption)
      .map(res => res);
  }

  getById(id: number): any {
    return this._http.get(this._actionUrl + id, this._configuration.resquestOption)
      .map(res => res.json());
  }

  save(categoria: Categoria) {
    let categoriaCopy = _.cloneDeep(categoria);
    delete categoriaCopy.idCategoria;
    return this._http.post(this._actionUrl, categoriaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(categoria: Categoria) {
    let categoriaCopy = _.cloneDeep(categoria);
    let id = categoriaCopy.idCategoria;
    delete categoriaCopy.idCategoria;
    return this._http.put(this._actionUrl + id, categoriaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(categoria: Categoria) {
    return this._http.delete(this._actionUrl + categoria.idCategoria, this._configuration.resquestOption).map(res => res.json());
  }

  handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }


}
