import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Mensaje} from '../models';
import * as _ from "lodash";

@Injectable()
export class MensajesService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'mensajes/';
  }

  getAll(): any {
    return this._http.get(this._actionUrl+'?perPage=99&sort=idMensaje:DESC', this._configuration.resquestOption)
      .map(res => res);
  }

  save(mensaje:Mensaje):any {
    let mensajeCopy = _.cloneDeep(mensaje);
    delete mensajeCopy.idMensaje;
    return this._http.post(this._actionUrl, mensajeCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(mensaje:Mensaje):any {
    let mensajeCopy = _.cloneDeep(mensaje);
    let id = mensaje.idMensaje;
    delete mensajeCopy.idMensaje;
    return this._http.put(this._actionUrl + id, mensajeCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(mensaje:Mensaje):any {
    let id = mensaje.idMensaje;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

}
