import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {Configuration} from "./config.service.ts";
import {Usuario} from "../models";
import * as _ from "lodash";

@Injectable()
export class UsuarioService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'usuarios/';
  }

  getAll(type: number = null, page:number = 1, perPage:number = 10): any {
    let actionUrl = this._actionUrl+'?page='+page+'&perPage='+perPage;
    if (type != null) {
      actionUrl += '&tipo=' + type;
    }
    return this._http.get(actionUrl, this._configuration.resquestOption)
      .map(res => res);
  }

  save(usuario:Usuario) {
    let usuarioCopy = _.cloneDeep(usuario);
    delete usuarioCopy.idUsuario;
    return this._http.post(this._actionUrl, usuarioCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(usuario:Usuario) {
    let usuarioCopy = _.cloneDeep(usuario);
    let id = usuarioCopy.idUsuario;
    delete usuarioCopy.idUsuario;
    return this._http.put(this._actionUrl + id, usuarioCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(usuario:Usuario) {
    return this._http.delete(this._actionUrl + usuario.idUsuario, this._configuration.resquestOption).map(res => res.json());
  }

  handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  login(login:any){
    return this._http.post(this._actionUrl + 'login', login, this._configuration.resquestOption)
      .map((response:any) => {
        let user = JSON.parse(response._body)[0];
        if (user && user.token) {
          localStorage.setItem('current', JSON.stringify(user));
          localStorage.setItem('token', user.token);
          localStorage.setItem('idUsuario', user.idUsuario);
          localStorage.setItem('nombre', user.nombre);
        }
      });
  }

  logout() {
    localStorage.removeItem('current');
    localStorage.removeItem('token');
    localStorage.removeItem('idUsuario');
    localStorage.removeItem('nombre');
    localStorage.removeItem('tipoUsuario');
    localStorage.removeItem('codId');
    localStorage.removeItem('ciudad');
    localStorage.removeItem('idPais');
  }

}
