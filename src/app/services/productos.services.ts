import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Producto} from '../models';
import * as _ from "lodash";

@Injectable()
export class ProductoService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'productos/';
  }

  getAll(page:number = 1, perPage:number = 10): any {
    let actionUrl = this._actionUrl+'?page='+page+'&perPage='+perPage;
    return this._http.get(actionUrl, this._configuration.resquestOption)
      .map(res => res);
  }

  getById(id: number): any {
    return this._http.get(this._actionUrl + id, this._configuration.resquestOption)
      .map(res => res.json());
  }

  save(producto: Producto) {
    let productoCopy = _.cloneDeep(producto);
    delete productoCopy.idProducto;
    return this._http.post(this._actionUrl, productoCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(producto: Producto) {
    let productoCopy = _.cloneDeep(producto);
    let id = productoCopy.idProducto;
    delete productoCopy.idProducto;
    return this._http.put(this._actionUrl + id, productoCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(producto: Producto) {
    return this._http.delete(this._actionUrl + producto.idProducto, this._configuration.resquestOption).map(res => res.json());
  }

  handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  deleteImage(idProducto:number, idImage:number) {
    return this._http.delete(this._actionUrl + idProducto+'/imagenes/'+idImage, this._configuration.resquestOption).map(res => res.json());
  }
}
