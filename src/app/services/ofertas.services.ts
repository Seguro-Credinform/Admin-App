import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Oferta} from '../models';
import * as _ from "lodash";

@Injectable()
export class OfertasService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'ofertas/';
  }

  getAll(): any {
    return this._http.get(this._actionUrl+'?perPage=99', this._configuration.resquestOption)
      .map(res => res);
  }

  save(oferta:Oferta):any {
    let ofertaCopy = _.cloneDeep(oferta);
    delete ofertaCopy.idOferta;
    return this._http.post(this._actionUrl, ofertaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(oferta:Oferta):any {
    let ofertaCopy = _.cloneDeep(oferta);
    let id = oferta.idOferta;
    delete ofertaCopy.idOferta;
    return this._http.put(this._actionUrl + id, ofertaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(oferta:Oferta):any {
    let id = oferta.idOferta;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

}
