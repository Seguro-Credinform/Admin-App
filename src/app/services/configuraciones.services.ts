import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Configuracion} from './../models';
import * as _ from "lodash";

@Injectable()
export class ConfiguracionesService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'configuraciones/';
  }

  get(): any {
    return this._http.get(this._actionUrl, this._configuration.resquestOption)
      .map(res => res);
  }

  edit(configuracion:Configuracion) {
    let configuracionCopy = _.cloneDeep(configuracion);
    let id = configuracion.idConfiguracion;
    delete configuracionCopy.idConfiguracion;
    return this._http.put(this._actionUrl + id, configuracionCopy, this._configuration.resquestOption).map(res => res.json());
  }

}
