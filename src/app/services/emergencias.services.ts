import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Emergencia} from '../models';
import * as _ from "lodash";

@Injectable()
export class EmergenciaService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'emergencias/';
  }

  getAll(): any {
    return this._http.get(this._actionUrl+'?perPage=99', this._configuration.resquestOption)
      .map(res => res);
  }

  save(emergencia:Emergencia):any {
    let emergenciaCopy = _.cloneDeep(emergencia);
    delete emergenciaCopy.idEmergencia;
    return this._http.post(this._actionUrl, emergenciaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(emergencia:Emergencia):any {
    let emergenciaCopy = _.cloneDeep(emergencia);
    let id = emergencia.idEmergencia;
    delete emergenciaCopy.idEmergencia;
    return this._http.put(this._actionUrl + id, emergenciaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(emergencia:Emergencia):any {
    let id = emergencia.idEmergencia;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

}
