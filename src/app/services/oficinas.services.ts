import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service.ts';
import {Oficina} from '../models';
import * as _ from "lodash";

@Injectable()
export class OficinasService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'oficinas/';
  }

  getAll(): any {
    return this._http.get(this._actionUrl+'?perPage=99', this._configuration.resquestOption)
      .map(res => res);
  }

  save(oficina:Oficina):any {
    let oficinaCopy = _.cloneDeep(oficina);
    delete oficinaCopy.idOficina;
    return this._http.post(this._actionUrl, oficinaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(oficina:Oficina):any {
    let oficinaCopy = _.cloneDeep(oficina);
    let id = oficina.idOficina;
    delete oficinaCopy.idOficina;
    return this._http.put(this._actionUrl + id, oficinaCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(oficina:Oficina):any {
    let id = oficina.idOficina;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

}
