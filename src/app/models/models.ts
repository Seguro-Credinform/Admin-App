/**
 * Configuracion general del sistema
 */
export class Configuracion {
  constructor(public idConfiguracion: number,
              public iva: number,
              public montoMaximoTransaccionRapida: number,
              public tiempoAtencionReferencial: number) {
  }
}

export class Error {
  constructor(public status: number,
              public message: string) {
  }
}

/**
 * Bancos
 */

export class Emergencia {
  constructor(public idEmergencia: number,
              public nombre: string,
              public telefono: string,
              public correo: string,
              public imagen: any,
              public status: number) {
  }
}

export class Informacion {
    constructor(public idInformacion: number,
                public titulo: string,
                public descripcion: string,
                public imagen: any,
                public status: number) {
    }
}

export class Oferta {
    constructor(public idOferta: number,
                public titulo: string,
                public descripcion: string,
                public imagen: any,
                public status: number) {
    }
}

export class Oficina {
    constructor(public idOficina: number,
                public nombre: string,
                public telefono: string,
                public correo: string,
                public direccion: string,
                public status: number) {
    }
}

export class Mensaje {
  constructor(public idMensaje: number,
              public asunto: string,
              public username: string,
              public mensaje: string,
              public respuesta: string,
              public idTipoMensaje: number) {
  }
}

/**
 * Modelo de Productos
 */
export class Producto {
  constructor(public idProducto: number,
              public idCategoria: number,
              public codigo: string,
              public nombre: string,
              public descripcion: string,
              public color: string,
              public marca: string,
              public medidas: string,
              public precio: number,
              public cantidad: number,
              public imagen: any,
              public status: number) {
  }
}

/**
 * Modelo de ubicacion
 */
export class Estado {
  constructor(public idEstado: number,
              public nombre: string,
              public status:number) {
  }
}

export class Ciudad {
  constructor(public idCiudad: number,
              public nombre: string,
              public status:number) {
  }
}


/**
 * Modelos de usuarios
 */
export class Usuario {
  constructor(public idUsuario: number,
              public nombre: string,
              public direccion: string,
              public email: string,
              public password: string,
              public telefono: string,
              public tipo: number,
              public tipoCliente: number,
              public docId: string,
              public status: number,
              public idCiudad: number) {
  }
}

/**
 * Modelos de categoria
 */
export class Categoria {
  constructor(public idCategoria: number,
              public nombre: string,
              public idCategoriaPadre: number,
              public permalink: string,
              public imagen: any,
              public status: number) {
  }
}

export class CategoriaNode {
  constructor(public datosCategoria: Categoria,
              public subCategorias: CategoriaNode[]) {
  }
}

/**
 * Modelo de preguntas frecuentes
 */
export class PreguntasFrecuentes {
  constructor(public idPregunta: number,
              public texto: string,
              public respuesta: string,
              public status: number) {
  }
}

/**
 * Modelo de Noticias
 */
export class Noticias {
  constructor(public idNoticia: number,
              public titulo: string,
              public resumen: string,
              public cuerpo: string,
              public imagen: any,
              public status: number) {
  }
}

